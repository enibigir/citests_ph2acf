#!/bin/bash

BoldWhite="\e[1;37m"
BoldMagenta="\e[1;35m"
ENDCOLOR="\e[0m"

new_job() { echo ""; echo -e " ${BoldMagenta}--- "; echo -e "  $1"; echo -e " ---${ENDCOLOR} "; echo ""; }

new_job "Installing gcc-toolset-10-annobin"
yum -y install gcc-toolset-10-annobin
ln -s /opt/rh/gcc-toolset-10/root/lib/gcc/x86_64-redhat-linux/10/plugin/annobin.so /opt/rh/gcc-toolset-10/root/lib/gcc/x86_64-redhat-linux/10/plugin/gcc-annobin.so

PH2OUTNAME=Test_Ph2_ACF

PH2VERSION=https://gitlab.cern.ch/cmsinnertracker/Ph2_ACF.git
#PH2COMMIT=bb9504499a65f47eb8d75fd2eae12e6028016c4b

#CONFIG=NONE or CompileWithTCUSB or CompileWithEUDAQ or ...
CONFIG=CompileWithEUDAQ

new_job "Download Ph2_ACF"
git clone --recurse-submodules $PH2VERSION $PH2OUTNAME; #git checkout $PH2COMMIT

if [ -d $PH2OUTNAME ];then
     new_job "Setting Ph2_ACF env"
     cd $PH2OUTNAME
     source setup.sh
     export $CONFIG=true
     echo "    CompileWithTCUSB=$CompileWithTCUSB"
     echo "    CompileWithEUDAQ=$CompileWithEUDAQ"
     cd ..

     if [ "$CompileWithTCUSB" = true ];then
          new_job "Download TCUSB"
          git clone https://gitlab.cern.ch/cms_tk_ph2/cmsph2_tcusb.git; #git checkout $TCUSBVERSION
          cd cmsph2_tcusb; mkdir build; cd build; cmake ..; make -j$(nproc); cd ../..
     fi

     new_job "Compiling Ph2_ACF"
     cd $PH2OUTNAME; mkdir build; cd build; 
     new_job "Running cmake"; time cmake ..;
     new_job "Running make";  time make -j$(nproc); 
     cd ../..

     new_job "Cleaning working directory"
     rm -rf $PH2OUTNAME
     if [ -d cmsph2_tcusb ];then rm -rf cmsph2_tcusb; fi
else
     echo "Failed to downlaod Ph2_ACF"
fi
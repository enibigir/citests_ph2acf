#!/bin/bash

REGISTRY=gitlab-registry.cern.ch/cms_tk_ph2/docker_exploration
IMAGE=$REGISTRY/cmstkph2_ci_c7:test
#cmstkph2_udaq_al8 cmstkph2_ci_al8

ENTRYPOINT='--entrypoint ./test_ph2acf.sh'

if [ $# -ne 1 ]; then
    docker run --rm -ti -v $PWD:$PWD -w $PWD $ENTRYPOINT $IMAGE
else
    docker run --rm -ti -v $PWD:$PWD -w $PWD --entrypoint bash $REGISTRY/$1
fi
